1. install Java 8
2. git clone https://mandarinka69@bitbucket.org/mandarinka69/template_melnyk_test_task_cucumber_serenity.git
3. Open template_melnyk_test_task_cucumber_serenity
4. Open terminal
5. Command for run test "mvn clean verify"
6. Generate report "mvn serenity:aggregate"
7. Go to template_melnyk_test_task_cucumber_serenity/target/site/serenity and open index.html in Chrome

8. If you use Jenkins, need add testng.xml and use command for run and generate report (5, 6)