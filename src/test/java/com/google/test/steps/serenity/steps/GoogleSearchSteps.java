package com.google.test.steps.serenity.steps;

import com.google.test.pages.MainPage;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class GoogleSearchSteps extends PageObject {
    @Steps
    MainPage mainPage;

    public void openPage(String url){
        getDriver().get(url);
    }

    public void inputText(String searchQuery){
        mainPage.getInputTextField().sendKeys(searchQuery);
    }

    public void pressButtonSubmit(){
        mainPage.getButtonSubmit().click();
    }

    public void openLinkResult(int numLink){
        mainPage.openLinkResult(numLink);
    }



}
