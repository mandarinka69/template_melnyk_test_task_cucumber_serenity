package com.google.test.steps.serenity.stepCucumber;

import com.google.test.steps.serenity.AssertionsSteps;
import com.google.test.steps.serenity.steps.GoogleSearchSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class DefaultCucumberSteps {

    @Steps
    GoogleSearchSteps searchSteps;

    @Steps
    AssertionsSteps assertionsSteps;


    @Given("Open page (.*)$")
    public void navigateToUrl(String url) {
        searchSteps.openPage(url);
    }

    @When("Input in search field (.*)")
    public void typeSearchQuery(String search_query){
        searchSteps.inputText(search_query);
    }

    @Then("Press on search button")
    public void clickOnSearchButton() {
        searchSteps.pressButtonSubmit();
    }



    @Then("Open search result number (.*)$")
    public void openSearchResult(int searchResultNumber) {
        searchSteps.openLinkResult(searchResultNumber);
    }

    @And("Verify that url title contains (.*)$")
    public void verifyThatTitleContainsSearchQuery(String searchQuery) {
        assertionsSteps.checkTitle(searchQuery);
    }

    @Then("Search  domain (.*) on pagination page number (.*)")
    public void searchExpectedDomainUntilPaginationNumber(String expectedDomain, int paginationNumber) {
        assertionsSteps.checkUrlsOnPagination(expectedDomain, paginationNumber);
    }

}
