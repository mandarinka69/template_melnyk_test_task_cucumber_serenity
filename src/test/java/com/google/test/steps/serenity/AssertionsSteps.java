package com.google.test.steps.serenity;


import com.google.test.pages.MainPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class AssertionsSteps {

    MainPage mainPage;


    @Step()
    public void checkTitle(String textSearch){
        Assert.assertTrue("Text "+textSearch+" not found in title", getDriver().getTitle().toLowerCase().contains(textSearch));
    }


    @Step()
    public void checkUrlsOnPagination(String textToFind, int paginationNumber) {

        List<WebElement> foundUrl = new ArrayList<>();
        for (int i = 0; i < paginationNumber; i++) {
            mainPage.getButtonGoToNextPage();
            mainPage.urlsOnSearchResultPage();
            for (WebElement webElement : mainPage.urlsOnSearchResultPage()) {

                if (webElement.getText().contains(textToFind)) {
                    foundUrl.add(webElement);
                }
            }
        }

        Assert.assertTrue("Domain " + textToFind+ " not found", foundUrl.size() >= 1);

    }

}
