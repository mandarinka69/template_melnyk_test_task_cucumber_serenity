package com.google.test.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MainPage extends PageObject {


    @FindBy(css = "input[type=\"text\"]")
    private WebElement inputTextField;

    @FindBy(css = "input[type=\"submit\"]")
    private WebElement buttonSubmit;

    @FindBy(css = "#pnnext > span:nth-child(2)")
    private WebElement buttonGoToNextPage;

    public WebElement openLinkResult(int resultNumber){
        return getDriver().findElement(By.xpath("(//*[@class='LC20lb'])[" + resultNumber + "]"));
    }


    public List<WebElement> urlsOnSearchResultPage() {
        return getDriver().findElements(By.cssSelector("[class=\"LC20lb\"]"));
    }

    public WebElement getButtonSubmit() {
        return buttonSubmit;
    }

    public WebElement getInputTextField() {
        return inputTextField;
    }

    public WebElement getButtonGoToNextPage() {
        return buttonGoToNextPage;
    }
}
