Feature: Check domain on search page

  Scenario Outline: Verify that there is expected domain on search results pages
    Given Open page <url>
    When Input in search field <search_query>
    Then Press on search button
    Then Search  domain <expected_domain> on pagination page number <page_number>
    Examples:
      | url                     | search_query | expected_domain                    | page_number |
      | https://www.google.com/ | automation   | testautomationday.com              | 6           |