Feature:  Check title on first find page

  Scenario Outline: Open link on search results page and verify that title contains searched word
    Given Open page <url>
    When Input in search field <search_query>
    Then Press on search button
    Then Open search result number <search_result_number>
    And Verify that url title contains <search_query>
    Examples:
      | url                     | search_query | search_result_number |
      | https://www.google.com/ | automation   | 1                    |
